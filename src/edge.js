const { Master } = require('@commonshost/server')
const { promisify } = require('util')
const PubNub = require('pubnub')
const pino = require('pino')
const { s3cmd } = require('./s3cmd')
const { join, dirname, resolve } = require('path')
const mkdirp = require('mkdirp')
const { fetch } = require('./helpers/fetch')
const { getAccessToken } = require('./helpers/auth0')
const { readdir, readFile, writeFile, unlink } = require('fs')
const isDomainName = require('is-domain-name')
const pAll = require('p-all')
const cloneDeep = require('lodash.clonedeep')
const { domainToASCII: toASCII } = require('url')
const rimraf = require('rimraf')
const uuidV5 = require('uuid/v5')
const { hostname } = require('os')

const NOT_MODIFIED = 304

function onStatus (pubnub, { category, operation }) {
  return new Promise((resolve, reject) => {
    const listener = {
      status: (status) => {
        pubnub.removeListener(listener)
        if (status.category === category || status.operation === operation) {
          resolve()
        } else if (status.error === true) {
          let error = new Error('PubNub error')
          if (status.errorData) {
            error = status.errorData.response
              ? status.errorData.response.error
              : status.errorData
          }
          reject(error)
        }
      }
    }
    pubnub.addListener(listener)
  })
}

function isSafeFilePath (filepath) {
  return typeof filepath === 'string' &&
    filepath.length > 0 &&
    resolve('/', filepath) === filepath
}

class Edge {
  constructor (options) {
    this.options = options
    this.log = pino({ level: options.log.level })
    this.log.trace('edge.constructor')
  }

  async syncStore (prefix) {
    this.log.trace('edge.syncBucket')
    if (typeof prefix !== 'string') throw new Error('Invalid S3 bucket prefix')
    if (!prefix.startsWith('/')) prefix = '/' + prefix
    if (!prefix.endsWith('/')) prefix = prefix + '/'
    const { s3 } = this.options
    const fromRemote = `s3://${s3.bucket}${prefix}`
    const toLocal = join(this.options.store.path, prefix)
    await promisify(mkdirp)(toLocal)
    await s3cmd(s3, '--delete-removed', 'sync', fromRemote, toLocal)
  }

  async getConfiguration (domain) {
    const url = `${this.options.core.api}/v2/sites/${domain}/configuration`
    const headers = {
      'authorization': `Bearer ${await getAccessToken(this.options.auth0)}`,
      'accept': 'application/json'
    }
    const response = await fetch(url, { headers })
    if (response.ok === false || response.status !== 200) {
      throw new Error('Failed to retrieve configuration')
    }
    const { configuration: host } = await response.json()
    host.domain = domain
    host.root = join(this.options.store.path, 'sites', domain, 'public')
    return host
  }

  async syncConfigurations () {
    this.log.trace('edge.syncConfigurations')
    const { path } = this.options.configurations
    await promisify(mkdirp)(path)
    const lastModifiedLog = join(path, '-last-modified.log')
    const url = `${this.options.core.api}/v2/configurations`
    const headers = {
      'authorization': `Bearer ${await getAccessToken(this.options.auth0)}`,
      'accept': 'application/json'
    }
    try {
      const timestamp = await promisify(readFile)(lastModifiedLog)
      if (timestamp.length > 0) {
        headers['if-modified-since'] = timestamp.toString()
      }
    } catch (error) {
      if (error.code !== 'ENOENT') {
        throw error
      }
    }

    const response = await fetch(url, { headers })
    if (response.headers.has('last-modified')) {
      const timestamp = response.headers.get('last-modified')
      await promisify(writeFile)(lastModifiedLog, timestamp)
    }

    if (response.status === NOT_MODIFIED) {
      return
    }

    if (response.ok === false) {
      throw new Error('Failed to sync configuration')
    }

    if (response.status === 200) {
      const configurations = await response.json()
      if (Array.isArray(configurations) === false) {
        throw new Error('Invalid response: Expected an array')
      }
      await Promise.all(
        configurations
          .filter(({ domain }) => isDomainName(domain))
          .map((options) => promisify(writeFile)(
            join(path, options.domain),
            JSON.stringify(options, null, 2)
          ))
      )
    }
  }

  async loadConfiguration () {
    this.log.trace('edge.loadConfiguration')
    const files = await promisify(readdir)(this.options.configurations.path)
    const hosts = await pAll(
      files
        .filter((file) => isDomainName(file))
        .map((domain) => async () => {
          this.log.trace(`loading ${domain}`)
          const filepath = join(this.options.configurations.path, domain)
          const raw = await promisify(readFile)(filepath)
          const { configuration: host } = JSON.parse(raw)
          host.domain = domain
          host.root = join(this.options.store.path, 'sites', domain, 'public')
          return host
        }),
      { concurrency: 100 }
    )
    return Object.assign(cloneDeep(this.options.server), { hosts })
  }

  async listen () {
    this.log.trace('edge.listen')
    await this.syncStore('sites')
    await this.syncConfigurations()
    this.server = new Master({
      options: await this.loadConfiguration(),
      generateCertificate: false,
      serveDefaultSite: false
    })
    await this.server.listen()
    this.pubnub = new PubNub({
      ssl: true,
      uuid:
        this.options.pubnub.uuid ||
        uuidV5(hostname(), uuidV5.DNS),
      publishKey: this.options.pubnub.publishKey,
      subscribeKey: this.options.pubnub.subscribeKey
    })
    this.pubnub.subscribe({ channels: this.options.pubnub.channels })
    await onStatus(this.pubnub, { category: 'PNConnectedCategory' })
    this.log.trace('PubNub connected')
    this.pubnubListener = {
      status: this.onStatus.bind(this),
      message: this.onMessage.bind(this)
    }
    this.pubnub.addListener(this.pubnubListener)
  }

  async close () {
    this.log.trace('edge.close')
    this.pubnub.removeListener(this.pubnubListener)
    this.pubnubListener = undefined
    this.pubnub.unsubscribeAll()
    await onStatus(this.pubnub, { operation: 'PNUnsubscribeOperation' })
    this.pubnub.stop()
    await this.server.close()
  }

  onStatus (status) {
    if (status.error === true) {
      const error = status.errorData.response
        ? status.errorData.response.error
        : status.errorData
      this.log.error(error)
    } else {
      const label = (status.category || status.operation)
        .replace(/(Category|Operation)$/, '')
        .replace(/([a-z])([A-Z])/, '$1 $2')
        .toLowerCase()
        .replace(/^pn/, 'PubNub ')
      this.log.info(label)
    }
  }

  async onMessage ({ message }) {
    this.log.trace(message, 'edge.pubnub.message')

    if (!('domain' in message) || typeof message.domain !== 'string') return
    message.domain = toASCII(message.domain).toLowerCase()
    if (!isDomainName(message.domain)) return

    message.root = join(
      this.options.store.path, 'sites', message.domain, 'public'
    )

    if (message.type === 'site-deploy') {
      // if (message.isNewDomain === true) {}
      if (message.hasNewFiles === true) {
        const prefix = `sites/${message.domain}/public`
        await this.syncStore(prefix)
      }
      if (message.hasNewConfiguration === true) {
        message.configuration = await this.getConfiguration(message.domain)
      }
    } else if (message.type === 'configuration-update') {
      message.configuration = await this.getConfiguration(message.domain)
    } else if (message.type === 'site-delete') {
      const { options } = this
      const site = join(options.store.path, 'sites', message.domain)
      await promisify(rimraf)(site)
      const configuration = join(options.configurations.path, message.domain)
      try {
        await promisify(unlink)(configuration)
      } catch (error) {
        if (error.code !== 'ENOENT') throw error
      }
    } else if (message.type === 'file-delete') {
      if (!Array.isArray(message.files) || message.files.length === 0) return
      await pAll(
        message.files
          .filter(isSafeFilePath)
          .map((file) => async () => {
            const filepath = join(message.root, file)
            try {
              await promisify(unlink)(filepath)
            } catch (error) {
              if (error.code !== 'ENOENT') throw error
            }
          }),
        { concurrency: 100 }
      )
    } else if (message.type === 'file-update') {
      if (!Array.isArray(message.files) || message.files.length === 0) return
      const { s3 } = this.options
      const baseUri = `s3://${s3.bucket}/sites/${message.domain}/public`
      await pAll(
        message.files
          .filter(isSafeFilePath)
          .map((file) => async () => {
            const filepath = join(message.root, file)
            const uri = file.startsWith('/')
              ? `${baseUri}${file}`
              : `${baseUri}/${file}`
            await promisify(mkdirp)(dirname(filepath))
            await s3cmd(s3, '--force', 'get', uri, filepath)
          }),
        { concurrency: 4 }
      )
    } else if (message.type === 'certificate-issue') {
      const prefix = `sites/${message.domain}/crypto`
      await this.syncStore(prefix)
    } else if (message.type === 'certificate-revoke') {
      const prefix = `sites/${message.domain}/crypto`
      const path = join(this.options.store.path, prefix)
      await promisify(rimraf)(path)
    } else {
      return
    }
    this.server.message(message)
  }
}

module.exports.Edge = Edge
