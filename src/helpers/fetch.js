const fetch = require('node-fetch')
const { Agent } = require('https')

const pattern = /^https?:\/\/(localhost|\[::1?]|127\.0\.0\.1|0\.0\.0\.0)(:\d+)?\//i
function isLoopback (url) {
  return pattern.test(url)
}

module.exports.fetch = (url, options = {}) => {
  if (options.agent === undefined) {
    options.agent = new Agent({
      rejectUnauthorized: !isLoopback(url),
      ecdhCurve: 'P-384:P-256'
    })
  }
  return fetch(url, options)
}
