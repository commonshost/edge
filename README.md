# edge 🌟

Keeps content, certificates, and configurations on the frontend servers synced with the backend stores.

Connects to PubNub API to receive notifications from Core.

Has read-only access to object store (AWS S3).

Has access to certain endpoints on the Core, for example reading domain configurations.
