const test = require('blue-tape')
const { _s3cmd: s3cmd } = require('../src/s3cmd')

test('Censor secrets in error output', async (t) => {
  const options = {
    s3cmd: '', // I.e. `which s3cmd` output when s3cmd is not installed
    accessKeyId: 'secretkeyid',
    secretAccessKey: 'secretaccesskey'
  }
  await t.shouldFail(s3cmd(options), /--access_key=XXXXXXXX/)
})
