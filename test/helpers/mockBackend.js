const { createServer } = require('https')
const { ephemeral } = require('tls-keygen')

const endpoints = [
  {
    method: 'GET',
    pathname: '/v2/configurations',
    body: []
  },
  {
    method: 'GET',
    pathname: '/v2/sites/example.net/configuration',
    body: {
      domain: 'example.net',
      configuration: {
        domain: 'example.net'
      }
    }
  }
]

module.exports.mockBackend = async function mockBackend () {
  const credentials = await ephemeral({ entrust: false })
  const server = createServer(credentials, (request, response) => {
    console.log(`${request.method} ${request.url}`)
    for (const { method, pathname, body } of endpoints) {
      if (request.method === method &&
        request.url === pathname
      ) {
        response.end(JSON.stringify(body))
        return
      }
    }
    response.statusCode = 404
    response.end()
  })
  return server
}
