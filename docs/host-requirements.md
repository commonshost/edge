# Host a Commons Host Host 🐑

To join the Commons Host CDN, you can deploy the Edge software on a variety of platforms. This document describes eligibility requirements.

## Requirements

### Internet Reachable IPv4 Address

The IPv4 address assigned by your ISP must be reachable by other users on the public Internet. Check your public IPv4 address using: https://www.whatismyip.com

If your IPv4 address is in a private range, it is not accessible to the rest of the Internet. The private IPv4 address ranges are:

- `192.168.0.0` to `192.168.255.255` (65,536 IP addresses)
- `172.16.0.0` to `172.31.255.255` (1,048,576 IP addresses)
- `10.0.0.0` to `10.255.255.255` (16,777,216 IP addresses)

Some ISPs, due to the IPv4 address shortage, only issue private addresses and effectively NAT their customers, leaving them unable to accept incoming connections. This is common on mobile providers but increasingly also the case with wired residential connections. In most situations a static IPv4 address may be obtained from most ISPs, typically for a one-time and/or monthly fee.

Only an IPv4 address is required. It is great to also offer IPv6 access, but sadly not all ISPs issue these addresses. Typically IPv6 is not behind an ISP-wide NAT. ISPs tend to issue not just one IPv6 address but a range of IPv6 addresses to each customer.

A static IP address is not necessary. A dynamic IP address is sufficient. Currently it is required to use a third-party dynamic DNS service. These services monitors your IP address and immediately update your hostname. In the future this will be handled by Commons Host. For now, consider using: https://www.noip.com

Most home routers have support for Dynamic DNS services. Alternatively you can run an agent/client software on another computer at home, just make sure this computer is always on the same network as the server.

### Incoming Connections

The following TCP ports are required:

- `22` - SSH: remote system maintenance, monitoring, and security checks
- `80` - HTTP: plain text to secure redirects
- `443` - HTTPS & HTTP/2: serving encrypted traffic

In the future also UDP ports `80` and `443` may be required, when QUIC support is added.

If the server is connected to a typical broadband router, it will be necessary to direct these ports from the router to the server. This can be done in several ways, and it is best to consult the documentation of your router. The following website provides tutorials for many router brands and models: https://portforward.com/router.htm

Techniques to selectively map public ports to a server on the private network may be referred to as Port Mapping or Port Forwarding. Another method is to place the server in a DMZ, meaning all incoming traffic, except that intended for a NAT session, is directed to the server.

### Outgoing Connections

The server needs unrestricted access to make outbound connections. This is necessary for system updates, connecting to the CDN core, listening for pub-sub notifications, cache fetches, time syncronisation, and more.

### Bandwidth

The throughput of the Internet connection should be at least 10 Mbps upload and download. The download speed is important when syncing the cache/ The upload speed is important when serving content to users.

The latency to the ISP is important. On fibre connections this is typically under 5ms or even 1 ms within the same city. On DSL or cable connections it may be 10-20 ms. When testing latency avoid using Wi-Fi since it can add several milliseconds latency and jitter in noisy, crowded spectrum environments.

Check your Internet connection throughput and latency using: http://www.speedtest.net

If your ISP has a fair use policy (FUP) with limited data transfer volume caps, you should monitor for any over-use. Support for bandwidth limits (speed and volume), or access scheduling, will be added eventually.

## Little Lamb Mk I

The tiny edge PoP server based on the HardKernel Odroid HC1 platform.

### Electrical

The server is rated to consume up to 20 Watt.

The included AC/DC adapter delivers 5.0V 4.0A DC output and supports 100-240V~50/60Hz 0.8A input. Certified adapters are available for EU, Korean, and US.

Adapters may be needed to match local wall power socket.

The DC input power is provided through a fairly common barrel connector. Alternative power supplies or connectors should be readily available from local or online electronics suppliers. Multiple servers can also share a single, larger power supply unit (PSU).

### Connectivity

An ethernet cable, either Cat5E or Cat6, with RJ45 connector. Should support the full speed of the Internet connection, up to 1 Gbps.

## Dedicated Machine

Coming soon

## Cloud VPS

Coming soon

## Docker

Coming soon
